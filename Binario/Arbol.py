from Binario.Nodo import Nodo


class Arbol:
    __Raiz__ = None
    __n__ = 0

    def __init__(self, x):
        self.__Raiz__ = Nodo(x);
        self.__n__ = 1

    def add(self, padre, hijo):
        dir_padre = self.__buscar(self.__Raiz__, padre)
        if dir_padre is not None:
            dir_hijo = self.__buscar(self.__Raiz__, hijo)
            if dir_hijo is None:
                if dir_padre.getHI() is None or dir_padre.getHD() is None:
                    if dir_padre.getHI() is None:
                        dir_padre.setHI( Nodo(hijo) )
                    else:
                        dir_padre.setHD( Nodo(hijo) )
                    self.__n__ = self.__n__ + 1
                else:
                    print("El padre ya tiene 2 hijos")
            else:
                print("El hijo ya existe")
        else:
            print("El padre no existe")

    def __buscar(self, nodo, x):
        if nodo is None:
            return None
        if nodo.getData() == x:
            return nodo
        hi = self.__buscar(nodo.getHI(), x)
        if hi is not None:
            return hi
        return self.__buscar(nodo.getHD(), x)


    def inOrden(self):
        print("INORDEN")
        if self.__Raiz__ is None:
            print("Arbol vacio")
        else:
            self.__inOrden1(self.__Raiz__)

    def __inOrden1(self, nodo):
        if nodo is not None:
            self.__inOrden1(nodo.getHI())
            print("[ " + str(nodo.getData()) + " ]")
            self.__inOrden1(nodo.getHD())

    def sumarNodos(self):
        return self.__sumarNodos1(self.__Raiz__)

    def __sumarNodos1(self, nodo):
       if nodo is None:
           return 0
       if nodo.esHoja():
           return nodo.getData()
       return self.__sumarNodos1(nodo.getHI()) + self.__sumarNodos1(nodo.getHD()) + nodo.getData()

    def sumarNodosPares(self):
        return self.__sumarNodosPares1(self.__Raiz__)
    def __sumarNodosPares1(self, nodo):
        if nodo is None:
            return 0

        if nodo.esHoja():
            if nodo.getData() % 2 == 0:
                return nodo.getData()
            return 0

        a = self.__sumarNodosPares1(nodo.getHI())
        b = self.__sumarNodosPares1(nodo.getHD())
        if nodo.getData() % 2 == 0:
            return a + b + nodo.getData()
        return a + b

    def cantidadHojas(self):
        return self.__cantidadHojas1(self.__Raiz__)
    def __cantidadHojas1(self, nodo):
        if nodo is None:
            return 0
        if nodo.esHoja():
            return 1
        a = self.__cantidadHojas1(nodo.getHI())
        b = self.__cantidadHojas1(nodo.getHD())
        return a + b

    def cantidadNodosConUnHijo(self):
        return self.__cantidadNodosConUnHijo1(self.__Raiz__)

    def __cantidadNodosConUnHijo1(self, nodo):
        if nodo is None:
            return 0
        if nodo.esHoja():
            return 0
        a = self.__cantidadNodosConUnHijo1(nodo.getHI())
        b = self.__cantidadNodosConUnHijo1(nodo.getHD())
        if nodo.cantidadHijos() == 1:
            return a + b + 1
        return a + b































