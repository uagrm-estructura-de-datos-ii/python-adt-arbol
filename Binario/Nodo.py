class Nodo:
    __Data__ = 0
    __HI__ = None
    __HD__ = None

    def __init__(self, Data):
        self.__Data__ = Data
        self.__HI__ = None
        self.__HD__ = None

    def setData(self, Data):
        self.__Data__ = Data;

    def setHI(self, HI):
        self.__HI__ = HI

    def setHD(self, HD):
        self.__HD__ = HD

    def getData(self):
        return self.__Data__

    def getHI(self):
        return self.__HI__

    def getHD(self):
        return self.__HD__

    def cantidadHijos(self):
        a = 0
        if self.__HI__ is not None:
            a = a + 1
        if self.__HD__ is not None:
            a = a + 1
        return a
    def esHoja(self):
        if self.cantidadHijos() == 0:
            return True
        return False