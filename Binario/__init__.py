from Binario.Arbol import Arbol

arbol = Arbol(50)
arbol.add(50, 25)
arbol.add(50, 10)
arbol.add(25, 29)
arbol.add(55, 10)
arbol.add(50, 15)
arbol.add(25, 10)
arbol.inOrden()

s = arbol.sumarNodos()
print("La suma de todos tus nodos : " + str(s))

s = arbol.sumarNodosPares()
print("La suma de todos tus nodos pares : " + str(s))

s = arbol.cantidadHojas()
print("La cantidad de hojas es : " + str(s))

s = arbol.cantidadNodosConUnHijo()
print("La cantidad de nodos con un hijo es : " + str(s))