class Nodo:
    __M__ = 4
    __Datos__ = []
    __Hijos__ = []

    def __init__(self):
        for i in range(1, self.__M__):
            self.__Datos__.append(-1)
            self.__Hijos__.append(None)
        self.__Hijos__.append(None)

    def setData(self, Data, pos):
        self.__Datos__[pos-1] = Data

    def setHijo(self, Hijo, pos):
        self.__Hijos__[pos-1] = Hijo

    def getData(self, pos):
        return self.__Datos__[pos-1]

    def getHijo(self, pos):
        return self.__Hijos__[pos-1]

    def cantidadHijos(self):
        a = 0
        for i in range(1, self.__M__ + 1):
            if self.__Hijos__[i-1] is not None:
                a = a + 1
        return a

    def cantidadDatos(self):
        a = 0
        for i in range(1, self.__M__):
            if self.__Datos__[i-1] != -1:
                a = a + 1
        return a

    def esHoja(self):
        if self.cantidadHijos() == 0:
            return True
        return False

    def getPosData(self):        
        if self.cantidadDatos() == self.__M__-1:
            return -1
        for i in range(1, self.__M__):
            if self.__Datos__[i - 1] == -1:
                return i
        return -1

    def getPosHijo(self, Data):
        for i in range(1, self.__M__):
            if self.__Datos__[i - 1] > Data:
                return i
        return self.__M__

    def insertarData(self, Data):
        pos = self.getPosData() - 1
        while pos >= 1:
            if(Data < self.__Datos__[pos - 1]):
                self.__Datos__[pos] = self.__Datos__[pos-1]
            else:
                self.__Datos__[pos] = Data
                return
            pos = pos - 1
        self.__Datos__[0] = Data

    def mostrar(self):
        print(self.__Datos__)