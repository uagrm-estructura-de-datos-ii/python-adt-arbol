from BST.Arbol import Arbol

arbol = Arbol()
arbol.add(50)
arbol.add(50)
arbol.add(25)
arbol.add(75)
arbol.add(10)
arbol.add(29)
arbol.add(75)

arbol.inOrden()

s = arbol.sumarNodos()
print("La suma de todos tus nodos : " + str(s))

s = arbol.sumarNodosPares()
print("La suma de todos tus nodos pares : " + str(s))

s = arbol.cantidadHojas()
print("La cantidad de hojas es : " + str(s))

s = arbol.cantidadNodosConUnHijo()
print("La cantidad de nodos con un hijo es : " + str(s))